import tensorflow as tf
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.utils import to_categorical
from spektral.layers import GCNConv
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dropout, Input
from tensorflow.keras import regularizers
from sklearn.metrics import classification_report

from build_graph import node_labels, build
from embeds import *


labels_dict = node_labels()
labels = list(labels_dict.values())
G = build()


nodeslist_emb, feat_emb, node_emb, name = read_EmbsCNI(['filename.emb'])      #Read emb or txt file
node_emb = np.array(node_emb)
F = node_emb.shape[1]       #the dimensions of node embeddings
N = len(nodeslist_emb) 


random_state = 33
def encode_labels(train_size, test_size, validation_size):   #Default = [0.5, 0.3, 0.2]
    
    labels_index = [l for l in range(len(labels))]
    train_limit = int(len(labels_index) * train_size)
    test_limit = int(len(labels_index) * (train_size+test_size))
    validation_limit = int(len(labels_index) * (train_size+test_size+validation_size))
    
    train_indices = labels_index[:train_limit]
  
    test_indices = labels_index[:]
    validation_indices = labels_index[test_limit:]
    
    train_enc = np.zeros((N,),dtype=bool)
    train_enc[train_indices] = True 
    
    test_enc = np.zeros((N,),dtype=bool)
    test_enc[test_indices] = True 
    
    val_enc = np.zeros((N,),dtype=bool)
    val_enc[validation_indices] = True
    
    return train_enc, test_enc, val_enc    


def adj_matrix():
    A = nx.adjacency_matrix(G)
    return A


def onehot_encode_label():
    label_encoder = LabelEncoder()
    label = label_encoder.fit_transform(feat_emb)
    label = to_categorical(label)
    return label, label_encoder.classes_
#labels_encoded, classes = onehot_encode_label()


def GCN_Node_Class():
    #HyperParameters Tuning
    A = adj_matrix()
    labels_encoded, classes = onehot_encode_label()
    train_enc, test_enc, val_enc = encode_labels(train_size=0.6, test_size=0.2, validation_size=0.2)
    num_classes = len(set(feat_emb))
    
    hyperP = {'channels':16, 'dropout':0.5, 'l2_reg':5e-4, 
              'learning_rate':1e-2, 'epochs':200, 'es_patience':10}
    
    # Preprocessing operations
    tf.random.set_seed(1)
    A = GCNConv.preprocess(A).astype('f4')
    
    # Model definition
    X_in = Input(shape=(F, ))
    fltr_in = Input((N, ), sparse=True)
    
    dropout_1 = Dropout(hyperP['dropout'])(X_in)
    graph_conv_1 = GCNConv(hyperP['channels'],
                             activation='tanh',  #relu, tanh
                             kernel_regularizer=regularizers.L2(hyperP['l2_reg']),
                             use_bias=False)([dropout_1, fltr_in])
    
    dropout_2 = Dropout(hyperP['dropout'])(graph_conv_1)
    graph_conv_2 = GCNConv(num_classes,
                             activation='softmax',
                             use_bias=False)([dropout_2, fltr_in])
    
    tf.random.set_seed(1)
    # Build model
    model = Model(inputs=[X_in, fltr_in], outputs=graph_conv_2)
    optimizer = tf.keras.optimizers.Adam(lr = hyperP['learning_rate'])
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',  #categorical_crossentropy, binary_crossentropy
                  weighted_metrics=['acc'])
    model.summary()

    validation_data = ([node_emb, A], labels_encoded, val_enc)
    model.fit([node_emb, A], labels_encoded, sample_weight=train_enc, 
              epochs=hyperP['epochs'], batch_size=N, validation_data=validation_data, shuffle=False, 
              callbacks=[tf.keras.callbacks.EarlyStopping(patience=hyperP['es_patience'], restore_best_weights=True)])
    
    # Evaluate model
    X_te = node_emb[test_enc]
    A_te = A[test_enc,:][:,test_enc]
    y_te = labels_encoded[test_enc]
    y_pred = model.predict([X_te, A_te], batch_size=N)
        
    classes=list(map(str,classes))
    report = classification_report(np.argmax(y_te,axis=1), np.argmax(y_pred,axis=1), target_names=classes)
    
    print('>>>>>>>>>>>', name, end='\n')
    print('GCN Classification Report: \n {}'.format(report))


#GCN_Node_Class()


    
    
    
    